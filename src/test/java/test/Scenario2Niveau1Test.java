package test;

import main.Controleur;
import main.Traitement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import test.utils.PrismeUtils;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.mockito.Mockito.*;

/**
 * Scénario 2 - L’objectif est de vérifier que les échanges de messages entre contrôleur fonctionnent correctement
 */
@ExtendWith(MockitoExtension.class)
public class Scenario2Niveau1Test {
    private Controleur controleur1;
    private Traitement traitement1;

    private Controleur controleur2;
    private Traitement traitement2;

    private final int ID1 = 1;
    private final int ID2 = 2;
    private final String IP = "127.0.0.1";
    private final int PORT = 8081;
    private final boolean TEST = true;

    private final String RMI_PRISME_URL = String.format("rmi://%s:%o/prisme", IP, PORT);
    private final String RMI_CONTROLEUR1_URL = String.format("rmi://%s:%o/controleur_%o", IP, PORT, ID1);
    private final String RMI_CONTROLEUR2_URL = String.format("rmi://%s:%o/controleur_%o", IP, PORT, ID2);

    /**
     * Lance le script<br>
     * Initialisation des fibres (sites)
     * @throws IOException
     */
    @BeforeEach
    public void setUp() throws IOException {
        PrismeUtils.runScriptToStartResource();

        BlockingQueue<Object> verrou1 = new LinkedBlockingQueue<>(1);
        BlockingQueue<Object> verrou2 = new LinkedBlockingQueue<>(1);

        try {
            LocateRegistry.createRegistry(PORT);
        } catch (RemoteException e) {
            System.out.println("Instance de registre déja existant");
        }

        try {
            Registry annuaire = LocateRegistry.getRegistry(IP, PORT);

            controleur1 = Mockito.spy(new Controleur(ID1, RMI_CONTROLEUR1_URL, annuaire, verrou1));
            traitement1 = Mockito.spy(new Traitement(ID1, RMI_PRISME_URL, annuaire, controleur1, verrou1, IP, Integer.parseInt(PrismeUtils.PORT_PRISME), TEST));

            controleur2 = Mockito.spy(new Controleur(ID2, RMI_CONTROLEUR2_URL, annuaire, verrou2));
            traitement2 = Mockito.spy(new Traitement(ID2, RMI_PRISME_URL, annuaire, controleur2, verrou2, IP, Integer.parseInt(PrismeUtils.PORT_PRISME), TEST));
        } catch(RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Il y a deux sites qui sont lancés.<br>
     * Le site 1 est racine et possède le jeton.
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Scénario 2 - L’objectif est de vérifier que les échanges de messages entre contrôleur fonctionnent correctement")
    public void fonctionnementEchangesEntreControleur() throws InterruptedException {
        // 1. Vérification de l'initialisation des variables des contrôleurs des site 2 et site 1
        Assertions.assertEquals(controleur1.getDernier(), 0);
        Assertions.assertEquals(controleur1.getSuivant(), 0);
        Assertions.assertFalse(controleur1.isDemande());
        Assertions.assertTrue(controleur1.isJeton());

        Assertions.assertEquals(controleur2.getDernier(), 1);
        Assertions.assertEquals(controleur2.getSuivant(), 0);
        Assertions.assertFalse(controleur2.isDemande());
        Assertions.assertFalse(controleur2.isJeton());

        controleur1.start();
        traitement1.start();

        Thread.sleep(1000);

        controleur2.start();
        traitement2.start();

        // 2. Demande de section critique par le site 2
        verify(controleur2, timeout(100).times(1)).demanderSectionCritique();

        // Vérifier variables site 2 + site 1
        Assertions.assertEquals(controleur2.getDernier(), 0);
        Assertions.assertTrue(controleur2.isDemande());

        // 3. Fin d’accès à la ressource critique par le site 2
        verify(controleur2, timeout(1000).times(1)).quitterSectionCritique();
    }
}

package test.utils;

import java.io.File;
import java.io.IOException;

/**
 * Classe permettant de lancer une ressource
 */
public class PrismeUtils {

    private static final String SCRIPT_PRISME_PATH = "src/test/resources";
    private static final String SCRIPT_PRISME_NAME = "./prisme";
    public static final String PORT_PRISME = "9123";

    /**
     * Lance le script pour démarrer la ressource (prisme)
     * @throws IOException
     */
    public static void runScriptToStartResource() throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder(SCRIPT_PRISME_NAME, PORT_PRISME);
        processBuilder.directory(new File(SCRIPT_PRISME_PATH));
        processBuilder.start();
    }
}

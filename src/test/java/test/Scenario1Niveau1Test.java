package test;

import main.Controleur;
import main.Traitement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import test.utils.PrismeUtils;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.mockito.Mockito.*;

/**
 * Scénario 1 - L’objectif est de tester le bon fonctionnement d’un site
 */
@ExtendWith(MockitoExtension.class)
public class Scenario1Niveau1Test {

    private Controleur controleur;
    private Traitement traitement;

    private final int ID = 1;
    private final String IP = "127.0.0.1";
    private final int PORT = 8081;
    private final boolean TEST = true;
    private final String RMI_PRISME_URL = String.format("rmi://%s:%o/prisme", IP, PORT);
    private final String RMI_CONTROLEUR_URL = String.format("rmi://%s:%o/controleur_%o", IP, PORT, ID);

    /**
     * Lance le script<br>
     * Initialisation des fibres (sites)
     * @throws IOException
     */
    @BeforeEach
    public void setUp() throws IOException {
        PrismeUtils.runScriptToStartResource();

        BlockingQueue<Object> verrou = new LinkedBlockingQueue<>(1);

        try {
            LocateRegistry.createRegistry(PORT);
        } catch (RemoteException e) {
            System.out.println("Instance de registre déja existant");
        }

        try {
            Registry annuaire = LocateRegistry.getRegistry(IP, PORT);

            controleur = Mockito.spy(new Controleur(ID, RMI_CONTROLEUR_URL, annuaire, verrou));
            traitement = new Traitement(ID, RMI_PRISME_URL, annuaire, controleur, verrou, IP, Integer.parseInt(PrismeUtils.PORT_PRISME), TEST);
        } catch(RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Il y a un seul site qui est lancé et qui peut accéder à la ressource.<br>
     * Le site 1 est racine et possède le jeton.
     * @throws InterruptedException
     */
    @Test
    @DisplayName("Scénario 1 - L’objectif est de tester le bon fonctionnement d’un site")
    public void fonctionnement1SiteTest() throws InterruptedException {
        // 1. Vérification l'initialisation des variables
        Assertions.assertEquals(0, controleur.getDernier());
        Assertions.assertEquals(0, controleur.getSuivant());
        Assertions.assertFalse(controleur.isDemande());
        Assertions.assertTrue(controleur.isJeton());

        // Run site
        controleur.start();
        traitement.start();

        // 2. Vérification des messages envoyées
        // Demande d'accès à la resources a bien été demandée
        verify(controleur, timeout(100).times(1)).demanderSectionCritique();
        Assertions.assertTrue(controleur.isDemande());
        // Signaler autorisation a bien été envoyé
        verify(controleur, timeout(100).times(1)).signalerAutorisation();
        // Fin de section critique a bien été envoyé
        verify(controleur, timeout(1500).times(1)).quitterSectionCritique();

        // 3. Vérification des variables du site 1
        Assertions.assertFalse(controleur.isDemande());
    }
}
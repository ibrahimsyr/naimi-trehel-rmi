package test;

import main.Controleur;
import main.Traitement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import test.utils.PrismeUtils;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.mockito.Mockito.*;

/**
 * Scénario 3 - Tester que l’ajout d’une fibre au répartiteur laser fonctionne
 */
@ExtendWith(MockitoExtension.class)
public class Scenario3Niveau2Test {

    private final int ID_1 = 1;
    private final int ID_2 = 2;
    private final String IP = "127.0.0.1";
    private final int PORT = 8081;
    private final boolean TEST = true;
    private final String RMI_PRISME_URL = String.format("rmi://%s:%o/prisme", IP, PORT);
    private final String RMI_CONTROLEUR1_URL = String.format("rmi://%s:%o/controleur_%o", IP, PORT, ID_1);
    private final String RMI_CONTROLEUR2_URL = String.format("rmi://%s:%o/controleur_%o", IP, PORT, ID_2);

    private Registry annuaire;

    /**
     * Lance le script<br>
     * Initialisation des fibres (sites)
     * @throws IOException
     */
    @BeforeEach
    public void setUp() throws IOException {
        PrismeUtils.runScriptToStartResource();

        BlockingQueue<Object> verrou1 = new LinkedBlockingQueue<>(1);

        try {
            LocateRegistry.createRegistry(PORT);
        } catch (RemoteException e) {
            System.out.println("Instance de registre déja existant");
        }

        try {
            annuaire = LocateRegistry.getRegistry(IP, PORT);

            Controleur controleur1 = new Controleur(ID_1, RMI_CONTROLEUR1_URL, annuaire, verrou1);
            Traitement traitement1 = new Traitement(ID_1, RMI_PRISME_URL, annuaire, controleur1, verrou1, IP, Integer.parseInt(PrismeUtils.PORT_PRISME), TEST);
            controleur1.start(); traitement1.start();
        } catch(RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Il y a 1 site qui est déja lancé.
     * @throws InterruptedException
     * @throws RemoteException
     */
    @Test
    @DisplayName("Scénario 3 - Tester que l’ajout d’une fibre au répartiteur laser fonctionne")
    public void branchementFibre() throws InterruptedException, RemoteException {
        // Le site tourne pendant 3 secondes
        Thread.sleep(3000);

        // Branchement d'un nouveau site (fibre)
        BlockingQueue<Object> verrou2 = new LinkedBlockingQueue<>(1);
        Controleur controleur2 = Mockito.spy(new Controleur(ID_2, RMI_CONTROLEUR2_URL, annuaire, verrou2));
        Traitement traitement2 = Mockito.spy(new Traitement(ID_2, RMI_PRISME_URL, annuaire, controleur2, verrou2, IP, Integer.parseInt(PrismeUtils.PORT_PRISME), TEST));
        controleur2.start(); traitement2.start();

        /**
         * Vérification que le site 2 arrive à accéder à la ressource critique
         */
        // Demande d'accès à la resources a bien été demandée
        verify(controleur2, timeout(1000).times(1)).demanderSectionCritique();
        // Fin de section critique a bien été envoyé
        verify(controleur2, timeout(3000).times(1)).quitterSectionCritique();
    }
}
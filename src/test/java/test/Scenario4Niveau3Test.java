package test;

import main.Controleur;
import main.ControleurInterface;
import main.Site;
import main.Traitement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import test.utils.PrismeUtils;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.mockito.Mockito.*;

/**
 * Scénario 4 - Tester que la suppression d’une fibre au répartiteur laser fonctionne
 */
@ExtendWith(MockitoExtension.class)
public class Scenario4Niveau3Test {

    private Controleur controleur1;
    private Traitement traitement1;

    private Controleur controleur2;
    private Traitement traitement2;

    private Controleur controleur3;
    private Traitement traitement3;

    private final int ID_1 = 1;
    private final int ID_2 = 2;
    private final int ID_3 = 3;
    private final String IP = "127.0.0.1";
    private final int PORT = 8081;
    private final boolean TEST = true;
    private final String RMI_PRISME_URL = String.format("rmi://%s:%o/prisme", IP, PORT);
    private final String RMI_CONTROLEUR1_URL = String.format("rmi://%s:%o/controleur_%o", IP, PORT, ID_1);
    private final String RMI_CONTROLEUR2_URL = String.format("rmi://%s:%o/controleur_%o", IP, PORT, ID_2);
    private final String RMI_CONTROLEUR3_URL = String.format("rmi://%s:%o/controleur_%o", IP, PORT, ID_3);

    private Registry annuaire;

    /**
     * Lance le script<br>
     * Initialisation des fibres (sites)
     * @throws IOException
     */
    @BeforeEach
    public void setUp() throws IOException {
        PrismeUtils.runScriptToStartResource();

        BlockingQueue<Object> verrou1 = new LinkedBlockingQueue<>(1);
        BlockingQueue<Object> verrou2 = new LinkedBlockingQueue<>(1);
        BlockingQueue<Object> verrou3 = new LinkedBlockingQueue<>(1);

        try {
            LocateRegistry.createRegistry(PORT);
        } catch (RemoteException e) {
            System.out.println("Instance de registre déja existant");
        }

        try {
            annuaire = LocateRegistry.getRegistry(IP, PORT);

            controleur1 = Mockito.spy(new Controleur(ID_1, RMI_CONTROLEUR1_URL, annuaire, verrou1));
            traitement1 = Mockito.spy(new Traitement(ID_1, RMI_PRISME_URL, annuaire, controleur1, verrou1, IP, Integer.parseInt(PrismeUtils.PORT_PRISME), TEST));
            controleur1.start(); traitement1.start();
            Thread.sleep(2000);
            controleur2 = new Controleur(ID_2, RMI_CONTROLEUR2_URL, annuaire, verrou2);
            traitement2 = new Traitement(ID_2, RMI_PRISME_URL, annuaire, controleur1, verrou2, IP, Integer.parseInt(PrismeUtils.PORT_PRISME), TEST);
            controleur2.start(); traitement2.start();
            Thread.sleep(2000);
            controleur3 = Mockito.spy(new Controleur(ID_3, RMI_CONTROLEUR3_URL, annuaire, verrou3));
            traitement3 = Mockito.spy(new Traitement(ID_3, RMI_PRISME_URL, annuaire, controleur3, verrou3, IP, Integer.parseInt(PrismeUtils.PORT_PRISME), TEST));
            controleur3.start(); traitement3.start();
        } catch(RemoteException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Il y a 3 sites qui sont déja lancés.
     * @throws InterruptedException
     * @throws RemoteException
     */
    @Test
    @DisplayName("Scénario 4 - Tester que la suppression d’une fibre au répartiteur laser fonctionne")
    public void debranchementFibre() throws InterruptedException, RemoteException {
        // Les sites tournent pendant 3 secondes
        Thread.sleep(3000);

        /**
         * Débranchement d'un site (uniquement lorsqu'il n'accède pas à la SC)
         */
        // Fin de section critique a bien été envoyé
        verify(controleur1, timeout(1500).atLeastOnce()).quitterSectionCritique();
        String url;
        if (!controleur1.isDemande() && !controleur1.isJeton()) {
            url = controleur1.getUrl();
        } else {
            url = controleur2.getUrl();
        }

        for (String boundName: annuaire.list()) {
            if (boundName.matches(String.format(".*%s_\\d+$", Site.RMI_URL_CONTROLEUR))) { // regex pour exclure l'URL du prisme
                try {
                    ControleurInterface controleur = (ControleurInterface) this.annuaire.lookup(boundName);
                    controleur.oublierControleur(url);
                } catch (NotBoundException | RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Vérification que le site 3 arrive à accéder à la ressource critique
         */
        // Demande d'accès à la resources a bien été demandée
        verify(controleur3, timeout(1000).atLeastOnce()).demanderSectionCritique();
        // Fin de section critique a bien été envoyé
        verify(controleur3, timeout(3000).atLeastOnce()).quitterSectionCritique();
    }
}
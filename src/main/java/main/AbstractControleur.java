package main;

import lombok.Getter;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.concurrent.BlockingQueue;

/**
 * Classe abstraite qui représente un controleur RMI et implémente les fonctionnalités génériques
 */
@Getter
public abstract class AbstractControleur extends Thread implements ControleurInterface {

    /**
     * Identifiant du site
     */
    private int idSite;
    /**
     * URL du controleur
     */
    private String url;
    /**
     * Verrou pour gérer l'accès à la ressource critique entre le contrôleur
     * et le processus de traitement
     */
    private BlockingQueue<Object> verrou;

    /**
     * Constructeur
     * @param id Numéro du site
     * @param url URL du contrôleur
     * @param annuaire Annuaire RMI
     * @param verrou Verrou entre Ai et Pi
     * @throws RemoteException
     */
    public AbstractControleur(int id, String url, Registry annuaire, BlockingQueue<Object> verrou) throws RemoteException {
        super("P_" + id);
        this.idSite = id;
        this.url = url;
        this.verrou = verrou;

        try {
            ControleurInterface controleur = (ControleurInterface) UnicastRemoteObject.exportObject(this, 0);
            annuaire.rebind(url, controleur);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Réceptionne de la demande d'entree en section critique de la part du processus métier
     */
    public abstract void demanderSectionCritique();

    /**
     * Signale l'autorisation d'entrer en section critique auprès du processus métier
     */
    public void signalerAutorisation() throws InterruptedException {
        System.out.printf("[%s] %s: \tSignalement de l'autorisation\n", Timestamp.from(Instant.now()), getName());
        verrou.put(new Object());
    }

    /**
     * Réceptionne la notification du processus métier à sa sortie de la section critique
     */
    public abstract void quitterSectionCritique();
}

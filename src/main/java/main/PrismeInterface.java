package main;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Définie les prototypes des méthodes utilisables par un processsus métier d'un site.
 * pour qu'il puisse envoyer un message au prisme.
 */
public interface PrismeInterface extends Remote {

    /**
     * Création de la connexion avec la ressource critique.<br>
     * Envoie des informations à la ressource critique.<br>
     * Fermeture de la connexion avec la ressource critique.
     * @param message Message à envoyer au prisme
     * @throws RemoteException
     */
    void envoyerInformations(String message) throws RemoteException;
}

package main;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Définie les prototypes des méthodes utilisables par un contrôleur d'un site
 * pour qu'il puisse envoyer des messages à un contrôleur d'un autre site en utilisant RMI.
 */
public interface ControleurInterface extends Remote {

    /**
     * Réceptionne la demande d'entrée en section critique de la part du procéssus métier
     * @throws InterruptedException
     * @throws RemoteException
     */
    void demanderSectionCritique() throws InterruptedException, RemoteException;

    /**
     * Signale l'autorisation d'entrer en section critique auprès du processus métier
     * @throws InterruptedException
     * @throws RemoteException
     */
    void signalerAutorisation() throws InterruptedException, RemoteException;

    /**
     * Réceptionne la notification du procéssus métier à sa sortie de la section critique
     * @throws InterruptedException
     * @throws RemoteException
     */
    void quitterSectionCritique() throws InterruptedException, RemoteException;

    /**
     * Réceptionne la notification d'un autre contrôleur de son besoin de la ressource partagée
     * @param id Identifiant d'un site qui demande à entrer en section critique
     * @throws RemoteException
     */
    void dem_SC(int id) throws RemoteException;

    /**
     * Réceptionne le jeton d'accès à la ressource partagée
     * @throws RemoteException
     */
    void jeton() throws RemoteException;

    /**
     * Enregistre l'URL d'un contrôleur distant
     * @param urlDistant l'URL à mémoriser
     * @throws RemoteException
     */
    void enregistrerControleur(String urlDistant) throws RemoteException;

    /**
     * Oublie l'URL d'un contrôleur distant
     * @param urlDistant l'URL à oublier
     * @throws RemoteException
     */
    void oublierControleur(String urlDistant) throws RemoteException;
}

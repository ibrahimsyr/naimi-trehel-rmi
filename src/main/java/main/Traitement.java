package main;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Random;
import java.util.concurrent.BlockingQueue;

/**
 * Classe exécutant le code du processus métier
 */
public class Traitement extends Thread implements PrismeInterface {

    /**
     * Interface contenant les méthodes pour communiquer avec le prisme
     */
    private final PrismeInterface prisme;
    /**
     * Interface contenant les méthodes pour communiquer avec un contrôleur
     */
    private final ControleurInterface controleur;
    /**
     * Verrou pour gérer l'accès à la ressource critique entre le contrôleur
     * et le processus de traitement
     */
    private final BlockingQueue<Object> verrou;

    /**
     * IP de la ressource
     */
    private final String ipRessource;
    /**
     * Port de la ressource
     */
    private final int portRessource;

    /**
     * Temps de temporisation lors de la fin de la communication
     * avec le prisme
     */
    private final int workDuration;
    /**
     * Mode test
     */
    private final boolean test;

    /**
     * Constructeur
     * @param idSite Numéro du site
     * @param url URL du prisme
     * @param annuaire Annuaire RMI
     * @param controleur Interface pour les méthodes de l'annuaire RMI
     * @param verrou Verrou entre Ai et Pi
     * @param ipRessource IP de la ressource
     * @param portRessource Port de la ressource
     * @param test Mode test
     * @throws RemoteException
     */
    public Traitement(int idSite, String url, Registry annuaire, ControleurInterface controleur, BlockingQueue<Object> verrou,
                      String ipRessource, int portRessource, boolean test) throws RemoteException {
        super("A_" + idSite);
        this.controleur = controleur;
        this.verrou = verrou;
        this.ipRessource = ipRessource;
        this.portRessource = portRessource;
        if (!test) this.workDuration = 300;
        else this.workDuration = 1000;
        this.test = test;

        this.prisme = (PrismeInterface) UnicastRemoteObject.exportObject(this, 0);
        annuaire.rebind(url, this.prisme);
    }

    /**
     * Coeur du processus de traitement chargé d'envoyer des messages
     * au prisme.
     */
    public void run() {
        if (!test) {
            while(true) work();
        } else {
            for (int i = 0; i < 1; i++) work();
        }
    }

    private void work() {
        // Créer un message aléatoire
        Random random = new Random();
        String generatedMessage = Long.toString (random.nextLong () & Long.MAX_VALUE, 36);

        try {
            controleur.demanderSectionCritique();
            verrou.take();
            prisme.envoyerInformations(generatedMessage);
            controleur.quitterSectionCritique();
        } catch (InterruptedException | RemoteException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void envoyerInformations(String message) {
        System.out.printf("[%s] %s: \tEnvoie des informations au prisme\n", Timestamp.from(Instant.now()), getName());
        Socket clientSocket = null;
        PrintWriter out = null;

        try {
            clientSocket = new Socket(ipRessource, portRessource);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            out.println(String.format("%s: %s", getName(), message));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                // Envoie du message de fin au prisme
                if (clientSocket != null && out != null) {
                    // Temporisation
                    try {
                        Thread.sleep(workDuration);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    out.println("END");
                }

                if (out != null) out.close();
                if (clientSocket != null) clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

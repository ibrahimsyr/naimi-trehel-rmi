package main;

import org.apache.commons.cli.*;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Classe qui lance un site avec un processus de traitement A et un processus de contrôle P
 */
public class Site {

	/**
	 * Constructeur
	 */
	private Site() {}

	/**
	 * Pattern RMI pour l'URL du prisme
	 */
	private static final String PATTERN_RMI_PRISME_URL = "rmi://%s:%s/%s";
	/**
	 * Pattern RMI pour l'URL du contrôleur
	 */
	public static final String PATTERN_RMI_CONTROLEUR_URL = "rmi://%s:%s/%s_%o";

	/**
	 * Constante RMI pour l'URL du prisme
	 */
	private static final String RMI_URL_PRISME = "prisme";
	/**
	 * Constante RMI pour l'URL du contrôleur
	 */
	public static final String RMI_URL_CONTROLEUR = "controleur";

	/**
	 * Option courte - Identifiant du site
	 */
	private static final String OPT_ID_SITE = "id";
	/**
	 * Option longue - Identifiant du site
	 */
	private static final String LONG_OPT_ID_SITE = "idSite";
	/**
	 * Option courte - IP de l'annuaire
	 */
	private static final String OPT_IP_ANNUAIRE = "ipA";
	/**
	 * Option longue - IP de l'annuaire
	 */
	private static final String LONG_OPT_IP_ANNUAIRE = "ipAnnuaire";
	/**
	 * Option courte - Port de l'annuaire
	 */
	private static final String OPT_PORT_ANNUAIRE = "portA";
	/**
	 * Option longue - Port de l'annuaire
	 */
	private static final String LONG_OPT_PORT_ANNUAIRE = "portAnnuaire";

	/**
	 * Option courte - IP de la ressource
	 */
	private static final String OPT_IP_RESSOURCE = "ipR";
	/**
	 * Option longue - IP de la ressource
	 */
	private static final String LONG_OPT_IP_RESSOURCE = "ipRessource";
	/**
	 * Option courte - Port de la ressource
	 */
	private static final String OPT_PORT_RESSOURCE = "portR";
	/**
	 * Option longue - Port de la ressource
	 */
	private static final String LONG_OPT_PORT_RESSOURCE = "portRessource";
	/**
	 * Option courte - Mode test
	 */
	private static final String OPT_TEST = "test";
	/**
	 * Option longue - Mode test
	 */
	private static final String LONG_OPT_TEST = "modeTest";

	/**
	 * Programme principal
	 * @param args Arguments du programme
	 */
    public static void main(String[] args) {
		Options options = new Options();
		options.addRequiredOption(OPT_ID_SITE, LONG_OPT_ID_SITE, true, "Numéro du site");
		options.addRequiredOption(OPT_IP_ANNUAIRE, LONG_OPT_IP_ANNUAIRE, true, "IP de l'annuaire RMI");
		options.addRequiredOption(OPT_PORT_ANNUAIRE, LONG_OPT_PORT_ANNUAIRE, true, "Port de l'annuaire RMI");
		options.addRequiredOption(OPT_IP_RESSOURCE, LONG_OPT_IP_RESSOURCE, true, "IP de la ressource");
		options.addRequiredOption(OPT_PORT_RESSOURCE, LONG_OPT_PORT_RESSOURCE, true, "Port de la ressource");
		options.addOption(OPT_TEST, LONG_OPT_TEST, true, "Mode test");

		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			e.printStackTrace();
			System.exit(1);
		}

    	int idSite = Integer.parseInt(cmd.getOptionValue(LONG_OPT_ID_SITE));
    	String ipAnnuaire = cmd.getOptionValue(LONG_OPT_IP_ANNUAIRE);
		int portAnnuaire = Integer.parseInt(cmd.getOptionValue(LONG_OPT_PORT_ANNUAIRE));

		String ipRessource = cmd.getOptionValue(LONG_OPT_IP_RESSOURCE);
		int portRessource = Integer.parseInt(cmd.getOptionValue(LONG_OPT_PORT_RESSOURCE));
		boolean test = Optional.of(Boolean.parseBoolean(cmd.getOptionValue(LONG_OPT_TEST))).orElse(false);

    	String urlPrisme = String.format(PATTERN_RMI_PRISME_URL, ipAnnuaire, portAnnuaire, RMI_URL_PRISME);
		String urlControleur = String.format(PATTERN_RMI_CONTROLEUR_URL, ipAnnuaire, portAnnuaire, RMI_URL_CONTROLEUR, idSite);

		BlockingQueue<Object> verrou = new LinkedBlockingQueue<>(1);

		try {
			LocateRegistry.createRegistry(portAnnuaire);
		} catch (RemoteException e) {
			System.out.println("Instance de registre déja existant");
		}

		try {
			Registry annuaire = LocateRegistry.getRegistry(ipAnnuaire, portAnnuaire);

			AbstractControleur controleur = new Controleur(idSite, urlControleur, annuaire, verrou);
			Traitement traitement = new Traitement(idSite, urlPrisme, annuaire, controleur, verrou, ipRessource, portRessource, test);

			controleur.start();
			traitement.start();
		} catch(RemoteException e) {
			e.printStackTrace();
		}
    }
}

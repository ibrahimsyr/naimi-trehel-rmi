package main;

import lombok.Getter;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * Contrôleur RMI qui gère l'accès d'un processus métier à une ressource critique
 */
@Getter
public class Controleur extends AbstractControleur {

    /**
     * Constantes fixées à la création du site
     */
    private final Registry annuaire;
    /**
     * Contient la liste des URL des contrôleurs à partir de leur identifiant de site
     */
    private Map<Integer, String> urlControleurs;

    /**
     * Indique si le site est la racine ou pas
     */
    private int dernier = 0;
    /**
     * Contient l'identifiant du prochain site à avoir accès à la ressource critique
     */
    private int suivant  = 0;
    /**
     * Le site veut avoir accès à la ressource critique
     */
    private boolean demande = false;
    /**
     * Le site possède le jeton
     */
    private boolean jeton = false;

    /**
     * Constructeur du contrôleur
     * @param idSite Numéro du site auquel est associé le contrôleur
     * @param url Url du contrôleur
     * @param annuaire Annuaire RMI
     * @param verrou Verrou entre le processus métier et le contrôleur pour gérer l'accès à la ressource critique
     * @throws RemoteException Exception associée à l'annuaire RMI
     */
    public Controleur(int idSite, String url, Registry annuaire, BlockingQueue<Object> verrou) throws RemoteException {
        super(idSite, url, annuaire, verrou);
        this.annuaire = annuaire;
        this.urlControleurs = new HashMap<>();

        // Donne par défaut le jeton au site 1
        if (idSite == 1) {
            this.jeton = true;
        } else {
            this.dernier = 1;
        }

        for (String boundName: annuaire.list()) {
            if (boundName.matches(String.format(".*%s_\\d+$", Site.RMI_URL_CONTROLEUR))) { // regex pour exclure l'URL du prisme
                try {
                    ControleurInterface controleur = (ControleurInterface) this.annuaire.lookup(boundName);
                    controleur.enregistrerControleur(url);
                } catch (NotBoundException | RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void demanderSectionCritique() {
        System.out.printf("[%s] %s: \tDemande de section critique enregistree\n", Timestamp.from(Instant.now()), getName());
        // Il ne faut pas que le site soit déjà demandeur
        if (!demande) {
            // Le contrôleur n'est pas la racine la racine
            if (dernier != 0) {
                // Récupère la fonction dans l'annuaire pour envoyer un message à P[dernier]
                try {
                    String urlDernier = getURLControleur(dernier);
                    ControleurInterface controleurDernier = (ControleurInterface) this.annuaire.lookup(urlDernier);
                    // envoie un message de demande de section critique à P[dernier]
                    controleurDernier.dem_SC(getIdSite());
                } catch (RemoteException | NotBoundException e) {
                    e.printStackTrace();
                }

                dernier = 0;
            }
            // Le site est racine et on possède le jeton
            else if (jeton) {
                // On autorise directement au processus métier l'accès à la ressource critique
                try {
                    signalerAutorisation();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            demande = true;
        }
    }

    @Override
    public void quitterSectionCritique() {
        System.out.printf("[%s] %s: \tFin de section critique\n", Timestamp.from(Instant.now()), getName());
        // Soit demande et jeton
        if (demande && jeton && suivant != 0) {
            //  Récupère la fonction dans l'annuaire pour envoyer un message à P[suivant]
            try {
                String urlSuivant = getURLControleur(suivant);
                ControleurInterface controleurSuivant = (ControleurInterface) this.annuaire.lookup(urlSuivant);
                // Envoie le jeton au processus le plus prioritaire qui demande la SC i.e P[suivant]
                controleurSuivant.jeton();
            } catch (RemoteException | NotBoundException e) {
                e.printStackTrace();
            }

            suivant = 0;
            jeton = false;
        }
        demande = false;
    }

    @Override
    public void dem_SC(int j) {
//        System.out.printf("[%s] %s: \tRéception de dem_SC(%s)\n", Timestamp.from(Instant.now()), getName(),j);
        // Réception d'une demande lorsque le site n'est pas racine
        if (dernier != 0) {
            // Récupère la fonction dans l'annuaire pour envoyer un message à P[dernier]
            try {
                String urlDernier = getURLControleur(dernier);
                ControleurInterface controleurDernier = (ControleurInterface) this.annuaire.lookup(urlDernier);
                // on fait circuler la demande à un processus qui a potentiellement le jeton i.e P[dernier]
                controleurDernier.dem_SC(j);
            } catch (RemoteException | NotBoundException e) {
                e.printStackTrace();
            }
            dernier = j;
        }
        // P[i] a obligatoirement le jeton, s'il n'est pas demandeur
        else if (!demande) {
            System.out.printf("[%s] %s: \tEnvoie du jeton à (%s)\n", Timestamp.from(Instant.now()), getName(),j);
            // Récupère la fonction dans l'annuaire pour envoyer un message à P[j]
            try {
                String urlJ = getURLControleur(j);
                ControleurInterface controleurJ = (ControleurInterface) this.annuaire.lookup(urlJ);
                // on envoie le jeton au demandeur de la SC i.e P[j]
                controleurJ.jeton();
            } catch (RemoteException | NotBoundException e) {
                e.printStackTrace();
            }
            // Le site ne possède plus de jeton
            jeton = false;
        }
        else {
            suivant = j;
        }

        dernier = j;
    }

    @Override
    public void jeton() {
        System.out.printf("[%s] %s: \tRéception d'un jeton\n", Timestamp.from(Instant.now()), getName());
        try {
            jeton = true;
            signalerAutorisation();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Enregistre le controleur et envoie son URL au nouveau controleur
     * @param urlDistant l'URL à mémoriser
     */
    @Override
    public void enregistrerControleur(String urlDistant) {
        List<String> strings = Arrays.asList(urlDistant.split("_"));
        int id = Integer.parseInt(strings.get(strings.size()-1));

        if (!urlControleurs.containsKey(id)) {
            System.out.printf("[%s] %s: \tEnregistrement du controleur avec l'URL %s\n",
                    Timestamp.from(Instant.now()),
                    getName(),
                    urlDistant);
            urlControleurs.put(id, urlDistant);

            // Send his own URL
            if (!urlDistant.equals(getUrl())) {
                try {
                    ControleurInterface controleurNouveau = (ControleurInterface) this.annuaire.lookup(urlDistant);
                    controleurNouveau.enregistrerControleur(getUrl());
                } catch (RemoteException | NotBoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void oublierControleur(String urlDistant) {
        if (!getUrl().equals(urlDistant)) {
            System.out.printf("[%s] %s: \tOubli du controleur avec l'URL %s\n",
                    Timestamp.from(Instant.now()),
                    getName(),
                    urlDistant);
            urlControleurs.values().removeIf(value -> value.contains(urlDistant));
        }
    }

    /**
     * Attente de l'enregistrement de l'URL du controleur s'il n'existe pas encore
     * @param id Identifiant du controleur
     * @return URL du controleur
     */
    private String getURLControleur(int id) {
        String urlControleur = urlControleurs.get(id);

        // On attend que l'URL soit enregistrée
        while (urlControleur == null) {
            try {
                sleep(100);
                urlControleur = urlControleurs.get(id);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return urlControleur;
    }
}

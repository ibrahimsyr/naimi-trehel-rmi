# Projet d'algorithmique répartie et parallélisme

Contrôle d'un répartiteur laser (fibres).

## Exécution (nécessite Maven)

`cd naimi-trehel-rmi`

1. Lancer le prisme

`./src/main/resources/prisme <portRessource>`

2. Lancer les sites (1 processus traitement et 1 processus contrôleur)

`mvn exec:java -Dexec.mainClass=Site -Dexec.args="-id <idSite> -ipA <ipAnnuaire> -portA <portAnnuaire> -ipR <ipRessource> -portR <portRessource>"`

### Exemple

`./src/main/resources/prisme 8082`

`mvn exec:java -Dexec.mainClass=Site -Dexec.args="-id 1 -ipA 127.0.0.1 -portA 8081 -ipR 127.0.0.1 -portR 8082"`

## Lancement des tests

`mvn -Dtest=Scenario1Niveau1Test test`

`mvn -Dtest=Scenario2Niveau1Test test`

`mvn -Dtest=Scenario3Niveau2Test test`

`mvn -Dtest=Scenario4Niveau3Test test`